class CreateStatements < ActiveRecord::Migration
  def change
    create_table :statements do |t|
      t.datetime :date
      t.string :reason
      t.integer :nb_km
      t.integer :taxe_coef
      t.integer :ride
      t.string :start
      t.string :arrival
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
